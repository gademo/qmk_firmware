#include QMK_KEYBOARD_H


extern uint8_t is_master;

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
#define _QWERTY 0
#define _LOWER 1
#define _RAISE 2
#define _GAME 3

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_QWERTY] = LAYOUT(
  //┌────────────┬────────────┬────────────┬────────────┬────────────┬────────────┐ ┌───────────┬────────────┬────────────┬────────────┬────────────┬────────────┐
      DF(_GAME),      KC_Q,        KC_W,        KC_E,        KC_R,        KC_T,        KC_Y,        KC_U,        KC_I,        KC_O,        KC_P,      _______,
  //├────────────┼────────────┼────────────┼────────────┼────────────┼────────────┤ ├───────────┼────────────┼────────────┼────────────┼────────────┼────────────┤
       _______,   LGUI_T(KC_A),LALT_T(KC_S),LCTL_T(KC_D),LSFT_T(KC_F),    KC_G,        KC_H,    RSFT_T(KC_J),RCTL_T(KC_K),RALT_T(KC_L),RGUI_T(KC_SCLN), _______,
  //├────────────┼────────────┼────────────┼────────────┼────────────┼────────────┤ ├───────────┼────────────┼────────────┼────────────┼────────────┼────────────┤
       _______,       KC_Z,        KC_X,        KC_C,        KC_V,        KC_B,        KC_N,        KC_M,      KC_COMM,      KC_DOT,     KC_SLSH,     _______,
  //└────────────┴────────────┴────────────┴────────────┴────────────┴────────────┘ └───────────┴────────────┴────────────┴────────────┴────────────┴────────────┘
  //                   ┌──────────────────┬──────────────────┬──────────────────┐     ┌──────────────────┬────────────┬─────────────────┐
                        LT(_LOWER,KC_SPC), LT(_RAISE,KC_TAB), LT(_LOWER,KC_SPC),       LT(_LOWER,KC_BSPC),   KC_ENT,   LT(_LOWER,KC_ENT)
  //                   └──────────────────┴──────────────────┴──────────────────┘     └──────────────────┴────────────┴─────────────────┘
  ),

  [_LOWER] = LAYOUT(
  //┌────────────┬────────────┬────────────┬────────────┬────────────┬────────────┐ ┌───────────┬────────────┬────────────┬────────────┬────────────┬────────────┐
       _______,     _______,      KC_GRV,     KC_MINS,      KC_EQL,     _______,      _______,       KC_1,        KC_2,        KC_3,      _______,     _______,
  //├────────────┼────────────┼────────────┼────────────┼────────────┼────────────┤ ├───────────┼────────────┼────────────┼────────────┼────────────┼────────────┤
       _______,   KC_LGUI,LALT_T(KC_LBRC),LCTL_T(KC_RBRC),LSFT_T(KC_BSLS),_______,    _______,   RSFT_T(KC_4),RCTL_T(KC_5),RALT_T(KC_6),RGUI_T(KC_DOT),_______,
  //├────────────┼────────────┼────────────┼────────────┼────────────┼────────────┤ ├───────────┼────────────┼────────────┼────────────┼────────────┼────────────┤
       _______,     _______,     KC_SCLN,     KC_QUOT,     KC_SLSH,     _______,      _______,       KC_7,        KC_8,        KC_9,        KC_0,      _______,
  //└────────────┴────────────┴────────────┴────────────┴────────────┴────────────┘ └───────────┴────────────┴────────────┴────────────┴────────────┴────────────┘
  //                   ┌──────────────────┬──────────────────┬──────────────────┐     ┌──────────────────┬────────────┬─────────────────┐
                             _______,           KC_ESC,            _______,                 KC_DEL,         _______,        _______
  //                   └──────────────────┴──────────────────┴──────────────────┘     └──────────────────┴────────────┴─────────────────┘
  ),

  [_RAISE] = LAYOUT(
  //┌────────────┬────────────┬────────────┬────────────┬────────────┬────────────┐ ┌───────────┬────────────┬────────────┬────────────┬────────────┬────────────┐
       _______,     _______,     KC_MPRV,     KC_MPLY,     KC_MNXT,     _______,      _______,     _______,     _______,     _______,     _______,     _______,
  //├────────────┼────────────┼────────────┼────────────┼────────────┼────────────┤ ├───────────┼────────────┼────────────┼────────────┼────────────┼────────────┤
       _______,     KC_LGUI,     KC_LALT,     KC_LCTL,     KC_LSFT,     _______,      KC_LEFT,     KC_DOWN,      KC_UP,      KC_RGHT,     _______,     _______,
  //├────────────┼────────────┼────────────┼────────────┼────────────┼────────────┤ ├───────────┼────────────┼────────────┼────────────┼────────────┼────────────┤
       _______,     _______,     KC_VOLD,     KC_MUTE,     KC_VOLU,     _______,      KC_HOME,     KC_PGDN,     KC_PGUP,     KC_END,     _______,      _______,
  //└────────────┴────────────┴────────────┴────────────┴────────────┴────────────┘ └───────────┴────────────┴────────────┴────────────┴────────────┴────────────┘
  //                   ┌──────────────────┬──────────────────┬──────────────────┐     ┌──────────────────┬────────────┬─────────────────┐
                             _______,            _______,           _______,                KC_CAPS,        _______,        _______
  //                   └──────────────────┴──────────────────┴──────────────────┘     └──────────────────┴────────────┴─────────────────┘
  ),

  [_GAME] = LAYOUT(
  //┌────────────┬────────────┬────────────┬────────────┬────────────┬────────────┐ ┌───────────┬────────────┬────────────┬────────────┬────────────┬────────────┐
       KC_TAB,        KC_Q,        KC_W,        KC_E,        KC_R,        KC_T,         KC_Y,        KC_U,        KC_I,        KC_O,        KC_P,     DF(_QWERTY),
  //├────────────┼────────────┼────────────┼────────────┼────────────┼────────────┤ ├───────────┼────────────┼────────────┼────────────┼────────────┼────────────┤
       KC_CAPS,       KC_A,        KC_S,        KC_D,        KC_F,        KC_G,         KC_H,        KC_J,        KC_K,        KC_L,      KC_SCLN,     _______,
  //├────────────┼────────────┼────────────┼────────────┼────────────┼────────────┤ ├───────────┼────────────┼────────────┼────────────┼────────────┼────────────┤
       KC_LSFT,       KC_Z,        KC_X,        KC_C,        KC_V,        KC_B,         KC_N,        KC_M,      KC_COMM,      KC_DOT,     KC_SLSH,     _______,
  //└────────────┴────────────┴────────────┴────────────┴────────────┴────────────┘ └───────────┴────────────┴────────────┴────────────┴────────────┴────────────┘
  //                   ┌──────────────────┬──────────────────┬──────────────────┐     ┌──────────────────┬────────────┬─────────────────┐
                             KC_ESC,           KC_LALT,             KC_SPC,                 KC_BSPC,         KC_ENT,        KC_DEL
  //                   └──────────────────┴──────────────────┴──────────────────┘     └──────────────────┴────────────┴─────────────────┘
  )
};

int RGB_current_mode;

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

// Setting ADJUST layer RGB back to default
void update_tri_layer_RGB(uint8_t layer1, uint8_t layer2, uint8_t layer3) {
  if (IS_LAYER_ON(layer1) && IS_LAYER_ON(layer2)) {
    layer_on(layer3);
  } else {
    layer_off(layer3);
  }
}

//SSD1306 OLED update loop, make sure to add #define SSD1306OLED in config.h
#ifdef SSD1306OLED

void matrix_init_user(void) {
  iota_gfx_init(!has_usb());   // turns on the display
}

// When add source files to SRC in rules.mk, you can use functions.
const char *read_layer_state(void);
const char *read_logo(void);
void set_keylog(uint16_t keycode, keyrecord_t *record);
const char *read_keylog(void);
const char *read_keylogs(void);

// const char *read_mode_icon(bool swap);
// const char *read_host_led_state(void);
// void set_timelog(void);
// const char *read_timelog(void);

void matrix_scan_user(void) {
   iota_gfx_task();
}

void matrix_render_user(struct CharacterMatrix *matrix) {
  if (is_master) {
    // If you want to change the display of OLED, you need to change here
    matrix_write_ln(matrix, read_layer_state());
    matrix_write_ln(matrix, read_keylog());
    //matrix_write_ln(matrix, read_keylogs());
    //matrix_write_ln(matrix, read_mode_icon(keymap_config.swap_lalt_lgui));
    //matrix_write_ln(matrix, read_host_led_state());
    //matrix_write_ln(matrix, read_timelog());
  } else {
    matrix_write(matrix, read_logo());
  }
}

void matrix_update(struct CharacterMatrix *dest, const struct CharacterMatrix *source) {
  if (memcmp(dest->display, source->display, sizeof(dest->display))) {
    memcpy(dest->display, source->display, sizeof(dest->display));
    dest->dirty = true;
  }
}

void iota_gfx_task_user(void) {
  struct CharacterMatrix matrix;
  matrix_clear(&matrix);
  matrix_render_user(&matrix);
  matrix_update(&display, &matrix);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
    set_keylog(keycode, record);
  }

  return true;
}
#endif//SSD1306OLED
